from django.http import HttpResponse, Http404

from django.shortcuts import render, get_object_or_404
from .models import Semester, Module, Coursework, Task
from django.views import generic
from .forms import TaskForm
# from .forms import TaskForm

# Create your views here.

# shows all the semesters
# def index(request):
#     all_modules = Module.objects.all()
#     context = { 'all_modules':all_modules}
#     return render(request,'studyplanner/index.html',context)
class IndexView(generic.ListView):
    template_name = 'studyplanner/index.html'
    context_object_name = 'all_modules'
    def get_queryset(self):
        all_modules = Module.objects.all()
        return all_modules


# shows all the courseworks of specified module
# in case the user is tring to access an invalid module with the module_id "/module_id/"
# it will throw an error 404 exeption
def module_details(request,module_id):
    m = get_object_or_404(Module,pk = module_id)
    all_courseworks = Coursework.objects.all()
    current_module = m.module_name
    current_code = m.module_code
    courseworks = []
    exams = []
    for cw in all_courseworks:
        if(cw.module_id == module_id):
            if(cw.cw_type == "cw"):
                courseworks.append(cw)
            elif(cw.cw_type == "exam"):
                exams.append(cw)

    context = { 'courseworks':courseworks,
                'exams':exams,
                'current_module':current_module,
                'current_code':current_code,
                'm':m,
                }
    return render(request,'studyplanner/detail.html',context)


def task_new(request,coursework_id):
    module = get_object_or_404( Module, pk=coursework_id)
    try:
        selected_choice = module.objects.get(pk=request.POST['choice'])
    except (KeyError, Coursework.DoesNotExist):
        # Redisplay the question voting form.
        return render(request, 'studyplanner/detail.html', {
            'error_message': "You didn't select a choice.",
        })