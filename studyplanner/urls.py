from django.conf.urls import url
from django.urls import path

from studyplanner import views

app_name = 'studyplanner'
urlpatterns = [
    # /studyplanner/
    path('', views.IndexView.as_view(), name='index'),
    # path('', views.IndexView.as_view(), name='index'),

    # /studyplanner/id/
    # (id,method controller,name of template)
    path('<int:module_id>/', views.module_details, name='detail'),

    # path('<int:module_id>/addTaskUrl',views.addtask)
    path('<int:coursework_id>/task_new/',views.task_new,name ='task_new')

]