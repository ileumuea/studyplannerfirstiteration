from django import forms

from .models import Task

class TaskForm(forms.Form):
    task_name = forms.CharField(label='Task name', max_length=100)

    def clean_task_name(self):
        data = self.cleaned_data['task_name']

        return data