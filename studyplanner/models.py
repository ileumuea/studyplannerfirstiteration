from django.db import models

# Create your models here.


class Semester(models.Model):
    semester_term = models.CharField(max_length=10)

    def __str__(self):
        return "Semester:%s" %(self.semester_term)


class Module(models.Model):
    semester = models.ForeignKey(Semester,on_delete=models.CASCADE)
    # id = models.IntegerField(primary_key=True)
    module_name = models.CharField(max_length=250)
    module_code = models.CharField(max_length=20)
    module_duration = models.CharField(max_length=20)

    def __str__(self):
        return "ID:%d      %s" % (self.id,self.module_name)


class Coursework(models.Model):
    module = models.ForeignKey(Module, on_delete=models.CASCADE,default="")
    # task = models.ForeignKey(Task,on_delete=models.CASCADE)
    # id = models.IntegerField(primary_key=True)
    cw_name = models.CharField(max_length=500)
    cw_date_set = models.DateField()
    cw_deadline = models.DateField()
    cw_weight = models.PositiveIntegerField()
    cw_type = models.CharField(max_length=10,default="")

    def __str__(self):
        return "ID:%s      %s" % (self.id,self.cw_name)

class Task(models.Model):
    coursework = models.ForeignKey(Coursework,on_delete=models.CASCADE,default="")
    task_name = models.CharField(max_length=500)
    task_assignment = models.CharField(max_length=250)
    task_date_set = models.DateField()
    task_notes = models.CharField(max_length=1000)
    task_criterion = models.PositiveIntegerField()
    task_type = models.CharField(max_length=50)

    def __str__(self):
        return "ID:%d   Task:%s"%(self.id,self.task_name)
